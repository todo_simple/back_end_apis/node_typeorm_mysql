import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class TodoSimple {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    body: string;

    @Column()
    header: string;

    @Column()
    id_user: number;

}
